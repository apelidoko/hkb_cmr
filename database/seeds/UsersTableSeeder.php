<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin User',
                'email' => 'admin@admin.com',
                'email_verified_at' => '2020-07-23 23:38:58',
                'password' => '$2y$10$28IoATcU5oa6Aye8z/8M/eHFGijIGs4TnuIQSC1DnC/2ZnVVq/1YO',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}