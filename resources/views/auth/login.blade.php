@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <!-- Modal HTML -->
            <div id="myModal" class="modal">
                <div class="modal-dialog modal-login modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">              
                            <h4 class="modal-title">{{ config('app.name', 'Laravel') }}</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <i class="fa fa-user"></i>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ trans('modules.email') }}" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <i class="fa fa-lock"></i>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ trans('modules.password') }}">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg">
                                       {{ trans('modules.login') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>      
@endsection

