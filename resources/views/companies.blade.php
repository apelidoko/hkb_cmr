@extends('layouts.frame')

@section('content')
<section class="content-header">
	<h1>
		{{ trans('modules.company.company_header') }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-list-alt"></i> {{ trans('modules.menu') }}</a></li>
		<li class="active">{{ trans('modules.company.company_header') }}</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class='col-md-12'>
			<div>
				<button  type="button" class="btn btn-success" data-toggle="modal" data-target="#CompanyModal">
					{{ trans('modules.company.company_add') }}
				</button>
			</div><br>
			<div class="panel-group">
			    <div class="panel panel-primary">
				      <div class="panel-heading"><i class="fa fa-list-alt"></i></div>
				      <div class="panel-body">
				      		<div class="table-responsive">
			                    <table class="display table table-bordered primary" id ="dataTable" name="dataTable">
			                        <thead>
			                            <tr class="border border-dark">
			                                <th>Name</th>
			                                <th>Email</th>
			                                <th>Website</th>
			                                <th>Logo</th>
			                                <th>Action</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	@foreach($companies as $company)
			                        		<tr id="{{ $company->id }}">
			                        			<td>{{ $company->name }}</td>
			                        			<td>{{ $company->email }}</td>
			                        			<td>{{ $company->website }}</td>
			                        			<td>
			                        				<img src='{{ asset("storage/" . $company->logo) }}' height="100" width="100"/>
		                        				</td>
			                        			<td>
			                        				<!-- Call to action buttons -->
		                                            <ul class="list-inline m-0">
		                                                <li class="list-inline-item">
															<button  type="button" data-toggle="modal" data-target="#CompanyEditModal" title="Add" class="btn btn-primary btn-sm rounded-0 companyBtn actionView">
															<i class="fa fa-eye"></i>
															</button>
		                                                </li>
		                                                <li class="list-inline-item">
		                                                    <button class="btn btn-success btn-sm rounded-0 companyBtn actionEdit" type="button" data-toggle="modal" data-target="#CompanyEditModal" title="Edit"><i class="fa fa-edit"></i></button>
		                                                </li>
		                                                <li class="list-inline-item">
		                                                    <button class="btn btn-danger btn-sm rounded-0 deleteCompany" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
		                                                </li>
		                                            </ul>
			                        			</td>
			                        		</tr>
			                        	@endforeach
			                        </tbody>
			                    </table>
		                	</div>
				      </div>
			    </div>
		    </div>			
		</div>
	</div>
</section>

<!-- Add Company Modal -->
<div class="modal fade" id="CompanyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title" id="exampleModalLabel">{{ trans('modules.company.company_add') }}</h5>
      </div>
      	<form name="uploadForm" id="uploadForm" method="POST" enctype="multipart/form-data">
		@csrf
	      <div class="modal-body">
            <div class="form-group">
	            <label for="company_name" class="col-form-label">Company Name:</label>
	            <input type="text" class="form-control" name="company_name" id="company_name" value="{{ old('company_name') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="email" class="col-form-label">Email:</label>
	            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
	          </div>
	          <div class="form-group">
	            <label for="logo" class="col-form-label">Logo:</label>
	            <input type="file" class="form-control" name="logo" id="logo" value="{{ old('logo') }}">
	          </div>
	          <div class="form-group">
	            <label for="website" class="col-form-label">Website:</label>
	            <input type="text" class="form-control" name="website" id="website" value="{{ old('website') }}">
	          </div>
			  <div class="bg-info txt-light storeAlertBar invisible" role="alert">
	          	<h4 class="txt-light">Company being Saved.. Please wait.. </h4>
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	         <input type="hidden" name="company_id" id="company_id" value="">
	         <input type="button" class="btn btn-primary" value="Save" name="submitForm">
	      </div>
    	</form>
    </div>
  </div>
</div>

<!-- View & Edit  Company Modal -->
<div class="modal fade" id="CompanyEditModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title" id="editModal"></h5>
      </div>
      	<form name="uploadForm_" id="uploadForm_" method="POST" enctype="multipart/form-data">
		@csrf
	      <div class="modal-body">
            <div class="form-group">
	            <label for="company_name_" class="col-form-label">Company Name:</label>
	            <input type="text" class="form-control" name="company_name_" id="company_name_" value="{{ old('company_name') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="email_" class="col-form-label">Email:</label>
	            <input type="email" class="form-control" name="email_" id="email_" value="{{ old('email') }}">
	          </div>
	          <div class="form-group">
	            <label for="logo_" class="col-form-label">Logo:</label>
	            <input type="file" class="form-control" name="logo_" id="logo_" value="{{ old('logo') }}">
	            <br/>
	            <label id="logo_filename"></label>
	            <center><img src="" id="logo_image" width="100" height="100" /></center>
	          </div>
	          <div class="form-group">
	            <label for="website_" class="col-form-label">Website:</label>
	            <input type="text" class="form-control" name="website_" id="website_" value="{{ old('website') }}">
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	         <input type="hidden" name="company_id_" id="company_id_" value="">
	         <input type="hidden" name="public_path" id="public_path" value="{{ asset('storage/') }}">
	         <input type="button" class="btn btn-primary" value="Update" name="editForm">
	      </div>
    	</form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
	$(function () {
		/*======================*/
		/* Highlight Menu Item */
		/*====================*/
		$("#company").addClass('active');

		/*=======================*/
		/* Initialize Datatable */
		/*=====================*/
		let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
		var dataTable = $('#dataTable').DataTable(
	    {
	    	"buttons": dtButtons,
	        "paging": true, // Allow data to be paged
	        "lengthChange": false,
	        "searching": true, // Search box and search function will be actived
	        "ordering": true,
	        "info": true,
	        "autoWidth": true,
	        "pageLength": 10,    // 5 rows per page
            "aoColumnDefs": [
                { 'bSortable': true }    ,
                { className: "dt-center", "aTargets": [0,1,2,3,4] },
            ],
            "columns": [
			    { "width": "20%" },
			    { "width": "20%" },
			    { "width": "20%" },
			    { "width": "20%" },
			    { "width": "20%" }
		    ]
	    });

		/*=====================*/
		/* Add Company Action */
		/*===================*/
	    $("form[name='uploadForm'] input[name='submitForm']").on("click", function(e){
			e.preventDefault();
			/* Input Data */
	    	var fileData = $('#logo').prop('files')[0];
	        var formData = new FormData($("form[name='uploadForm']")[0]); 
	        formData.append('file', fileData);  
	        /* Store Data */
	    	$.ajax({
		        url: "{{ route('companies.store') }}",
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		        dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,                         
                type: 'POST',
                beforeSend: function(){
                	$(".storeAlertBar").removeClass("invisible");
                },
		        success: function(data){
		        	$(".storeAlertBar").addClass("invisible");
		        	alert('Successfully Saved!');
		        	location.reload();
		        },
		        error: function(error){
		        	var err = error.responseJSON.errors;
		        	var output = "Error \n";
		        	$.each(err, function(a,b){
		        		output += b + "\n";
		        	});
		        	alert(output);
		        }
	      	});
	    });

	    /*=======================*/
		/* Display Company Data */
		/*=====================*/
	    $(document).on("click", ".companyBtn", function(){
	    	var $elem = $(this);
	    	var companyID = $elem.parents().eq(3).attr('id');
	    	var action;
	    	var isActionView = $(this).hasClass("actionView");
	    	/* If View Button is Clicked */
	    	if(isActionView)
    		{
    			action = "View Company";
    			$("input[name='editForm']").hide();
    			$("#CompanyEditModal :input[type='text'], #CompanyEditModal :input[type='email'] ").prop("disabled", true);
    		}else{
    			action = "Edit Company";
    			$("input[name='editForm']").show();
    			$("#CompanyEditModal :input").prop("disabled", false);
    		}

	    	$("#editModal").text(action);
	    	$.ajax({
		        url: "companies/" + companyID,    
                method: 'GET',
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		        dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,    
		        success: function(data){

		        	var company_name = data.name;
		        	var email = data.email;
		        	var logo = data.logo;
		        	var website = data.website;
		        	var company_id = data.id;
		        	var public_path = $("#public_path").val();
		        	var image_fullpath = public_path + '/' + logo;

		        	$("#company_name_").val(company_name);
		        	$("#email_").val(email);
		        	$("#logo_image").attr("src", image_fullpath);
		        	$("#logo_filename").text("File Name: " + logo);
		        	$("#website_").val(website);
		        	$("#company_id_").val(company_id);
		        }
	      	});
	    });

	    /*======================*/
		/* Update Company Data */
		/*====================*/
	    $("form[name='uploadForm_'] input[name='editForm']").on("click", function(e){
	    	e.preventDefault();
			/* Input Data */
	    	var fileData = $('#logo_').prop('files')[0];
	        var formData = new FormData($("form[name='uploadForm_']")[0]); 
	        var companyID = $("#company_id_").val();
		    formData.append('file', fileData); 
	        formData.append('_method','PUT');
	        /* Standardize Keys for Laravel Validation */
			for (var key of formData.keys()) {
				var inputName = key;
				var inputNameEndsWith_ = inputName.endsWith("_");
				if(inputNameEndsWith_)
				{
					inputName = key.slice(0,-1);
				}
				formData.set(inputName, formData.get(key));
			}
	        /* Update Data */
	    	$.ajax({
		        url: "companies/" + companyID,
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
                cache: false,
                contentType: false,
                processData: false,
                data: formData,                         
                method: 'POST',
		        success: function(data){
		        	alert('Successfully Updated!');
		        	location.reload();
		        },
		        error: function(error){
		        	var err = error.responseJSON.errors;
		        	var output = "Error \n";
		        	$.each(err, function(a,b){
		        		output += b + "\n";
		        	});
		        	alert(output);
		        }
	      	});
	    });

		/*=================*/
		/* Delete Company */
		/*===============*/
		$(document).on("click", ".deleteCompany", function(){
	    	var $elem = $(this);
	    	var companyID = $elem.parents().eq(3).attr('id');
			if (confirm("Deleting this Record will affect affiliated Employees, Proceed?")) {
				$.ajax({
					url: "companies/" + companyID,    
					method: 'DELETE',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					dataType: 'text',
					success: function(data){
						console.log(data);
						location.reload();
					}
				});
			} else {
				return;
			}
    	});
	});
</script>
@endsection