
@extends('layouts.frame')

@section('content')
<section class="content-header">
	<h1>
		{{ trans('modules.employees.employee_header') }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-list-alt"></i> {{ trans('modules.menu') }}</a></li>
		<li class="active">{{ trans('modules.employees.employee_header') }}</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class='col-md-12'>
			<div>
				<button  type="button" class="btn btn-success" data-toggle="modal" data-target="#EmployeeModal">
					{{ trans('modules.employees.employee_add') }}
				</button>
			</div><br>
			<div class="panel-group">
			    <div class="panel panel-primary">
				      <div class="panel-heading"><i class="fa fa-list-alt"></i></div>
				      <div class="panel-body">
				      		<div class="table-responsive">
			                    <table class="display table table-bordered primary" id ="dataTable" name="dataTable">
			                        <thead>
			                            <tr class="border border-dark">
			                                <th>First Name</th>
			                                <th>Last Name</th>
			                                <th>Company</th>
			                                <th>Email</th>
			                                <th>Phone</th>
			                                <th>Action</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	@foreach($employees as $employee)
			                        		<tr id="{{ $employee->employee_id }}">
			                        			<td>{{ $employee->firstname }}</td>
			                        			<td>{{ $employee->lastname }}</td>
			                        			<td>{{ $employee->name }}</td>
			                        			<td>{{ $employee->email }}</td>
			                        			<td>{{ $employee->phone }}</td>
			                        			<td>
			                        				<!-- Call to action buttons -->
		                                            <ul class="list-inline m-0">
		                                                <li class="list-inline-item">
															<button  type="button" data-toggle="modal" data-target="#EmployeeEditModal" title="Add" class="btn btn-primary btn-sm rounded-0 employeeBtn actionView">
															<i class="fa fa-eye"></i>
															</button>
		                                                </li>
		                                                <li class="list-inline-item">
		                                                    <button class="btn btn-success btn-sm rounded-0 employeeBtn actionEdit" type="button" data-toggle="modal" data-target="#EmployeeEditModal" title="Edit"><i class="fa fa-edit"></i></button>
		                                                </li>
		                                                <li class="list-inline-item">
		                                                    <button class="btn btn-danger btn-sm rounded-0 deleteEmployee" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
		                                                </li>
		                                            </ul>
			                        			</td>
			                        		</tr>
			                        	@endforeach
			                        </tbody>
			                    </table>
		                	</div>
				      </div>
			    </div>
		    </div>			
		</div>
	</div>
</section>

<!-- Add Employee Modal -->
<div class="modal fade" id="EmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title" id="exampleModalLabel">{{ trans('modules.employees.employee_add') }}</h5>
      </div>
      	<form name="uploadForm" id="uploadForm" method="POST" enctype="multipart/form-data">
		@csrf
	      <div class="modal-body">
              <div class="form-group">
	            <label for="firstname" class="col-form-label">FirstName:</label>
	            <input type="text" class="form-control" name="firstname" id="firstname" value="{{ old('firstname') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="lastname" class="col-form-label">LastName:</label>
	            <input type="text" class="form-control" name="lastname" id="lastname" value="{{ old('lastname') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="company" class="col-form-label">Company:</label>
	            <select name="company" id="company" class="form-control">
	            	<option value="">-Select Company-</option>
	            	@foreach($companies as $company)
	            		<option value="{{ $company->id }}">{{ $company->name }}</option>
	            	@endforeach
            	</select>
	          </div>
	          <div class="form-group">
	            <label for="email" class="col-form-label">Email:</label>
	            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
	          </div>
	          <div class="form-group">
	            <label for="phone" class="col-form-label">Phone:</label>
	            <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone') }}">
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	         <input type="hidden" name="employee_id" id="employee_id" value="">
	         <input type="button" class="btn btn-primary" value="Save" name="submitForm">
	      </div>
    	</form>
    </div>
  </div>
</div>

<!-- View & Edit Employee Modal -->
<div class="modal fade" id="EmployeeEditModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title" id="editModal"></h5>
      </div>
      	<form name="uploadForm_" id="uploadForm_" method="POST" enctype="multipart/form-data">
		@csrf
	      <div class="modal-body">
              <div class="form-group">
	            <label for="firstname_" class="col-form-label">FirstName:</label>
	            <input type="text" class="form-control" name="firstname_" id="firstname_" value="{{ old('firstname') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="lastname_" class="col-form-label">LastName:</label>
	            <input type="text" class="form-control" name="lastname_" id="lastname_" value="{{ old('lastname') }}" required>
	          </div>
	          <div class="form-group">
	            <label for="company_" class="col-form-label">Company:</label>
	            <select name="company_" id="company_" class="form-control">
	            	<option value="">-Select Company-</option>
	            	@foreach($companies as $company)
	            		<option value="{{ $company->id }}">{{ $company->name }}</option>
	            	@endforeach
            	</select>
	          </div>
	          <div class="form-group">
	            <label for="email_" class="col-form-label">Email:</label>
	            <input type="email" class="form-control" name="email_" id="email_" value="{{ old('email') }}">
	          </div>
	          <div class="form-group">
	            <label for="phone_" class="col-form-label">Phone:</label>
	            <input type="text" class="form-control" name="phone_" id="phone_" value="{{ old('phone') }}">
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	         <input type="hidden" name="employee_id_" id="employee_id_" value="">
	         <input type="button" class="btn btn-primary" value="Update" name="editForm">
	      </div>
    	</form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
	$(function () {
		/*======================*/
		/* Highlight Menu Item */
		/*====================*/
		$("#employee").addClass('active');

		/*=======================*/
		/* Initialize Datatable */
		/*=====================*/
		let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
		var dataTable = $('#dataTable').DataTable(
	    {
	    	"buttons": dtButtons,
	        "paging": true, // Allow data to be paged
	        "lengthChange": false,
	        "searching": true, // Search box and search function will be actived
	        "ordering": true,
	        "info": true,
	        "autoWidth": true,
	        "pageLength": 10,    // 5 rows per page
            "aoColumnDefs": [
                { 'bSortable': true }    ,
                { className: "dt-center", "aTargets": [0,1,2,3,4,5] },
            ],
            "columns": [
			    { "width": "15%" },
			    { "width": "20%" },
			    { "width": "20%" },
			    { "width": "20%" },
			    { "width": "10%" },
			    { "width": "15%" }
		    ]
	    });

		/*======================*/
		/* Add Employee Action */
		/*=============-======*/
	    $("form[name='uploadForm'] input[name='submitForm']").on("click", function(e){
			e.preventDefault();
			/* Input Data */
	        var formData = new FormData($("form[name='uploadForm']")[0]); 
	        /* Store Data */
	    	$.ajax({
		        url: "{{ route('employees.store') }}",
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		        dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,                         
                type: 'POST',
		        success: function(data){
		        	alert('Successfully Saved!');
		        	location.reload();
		        },
		        error: function(error){
		        	var err = error.responseJSON.errors;
		        	var output = "Error \n";
		        	$.each(err, function(a,b){
		        		output += b + "\n";
		        	});
		        	alert(output);
		        }
	      	});
	    });

	    /*========================*/
		/* Display Employee Data */
		/*======================*/
	    $(document).on("click", ".employeeBtn", function(){
	    	var $elem = $(this);
	    	var employeeID = $elem.parents().eq(3).attr('id');
	    	var action;
	    	var isActionView = $(this).hasClass("actionView");
	    	/* If View Button is Clicked */
	    	if(isActionView)
    		{
    			action = "View Employee";
    			$("input[name='editForm']").hide();
    			$("#EmployeeEditModal :input[type='text'], #EmployeeEditModal :input[type='email'] ").prop("disabled", true);
    		}else{
    			action = "Edit Employee";
    			$("input[name='editForm']").show();
    			$("#EmployeeEditModal :input").prop("disabled", false);
    		}

	    	$("#editModal").text(action);
	    	$.ajax({
		        url: "employees/" + employeeID,    
                method: 'GET',
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		        dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,    
		        success: function(data){

		        	var firstname = data.firstname;
		        	var lastname = data.lastname;
		        	var company_id = data.company_id;
		        	var email = data.email;
		        	var phone = data.phone;
		        	var employee_id = data.id;

		        	$("#firstname_").val(firstname);
		        	$("#lastname_").val(lastname);
		        	$("#company_ option[value="+company_id+"]").attr('selected','selected');
		        	$("#email_").val(email);
		        	$("#phone_").val(phone);
		        	$("#employee_id_").val(employee_id);
		        }
	      	});
	    });

	    /*=======================*/
		/* Update Employee Data */
		/*=====================*/
	    $("form[name='uploadForm_'] input[name='editForm']").on("click", function(e){
	    	e.preventDefault();
			/* Input Data */
	        var formData = new FormData($("form[name='uploadForm_']")[0]); 
	        var employeeID = $("#employee_id_").val();
	        formData.append('_method','PUT');
	        /* Standardize Keys for Laravel Validation */
			for (var key of formData.keys()) {
				var inputName = key;
				var inputNameEndsWith_ = inputName.endsWith("_");
				if(inputNameEndsWith_)
				{
					inputName = key.slice(0,-1);
				}
				formData.set(inputName, formData.get(key));
			}
	        /* Update Data */
	    	$.ajax({
		        url: "employees/" + employeeID,
		        headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
                cache: false,
                contentType: false,
                processData: false,
                data: formData,                         
                method: 'POST',
		        success: function(data){
		        	alert('Successfully Updated!');
		        	location.reload();
		        },
		        error: function(error){
		        	var err = error.responseJSON.errors;
		        	var output = "Error \n";
		        	$.each(err, function(a,b){
		        		output += b + "\n";
		        	});
		        	alert(output);
		        }
	      	});
	    });
		
		/*==================*/
		/* Delete Employee */
		/*================*/
		$(document).on("click", ".deleteEmployee", function(){
	    	var $elem = $(this);
	    	var employeeID = $elem.parents().eq(3).attr('id');
			if (confirm("Delete this Record?")) {
				$.ajax({
					url: "employees/" + employeeID,    
					method: 'DELETE',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					dataType: 'text',
					success: function(data){
						console.log(data);
						location.reload();
					}
				});
			} else {
				return;
			}
    	});
	});
</script>
@endsection