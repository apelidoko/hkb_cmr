
<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left">
        <i class="fa fa-industry"></i>
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
      </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">{{ trans('modules.menu') }}</li>
      <li id="company">
        <a href="{{ route('companies.index') }}">
          <i class="fa fa-list-alt"></i> <span>{{ trans('modules.company.company_header') }}</span>
        </a>
      </li>
      <li id="employee">
        <a href="{{ route('employees.index') }}">
          <i class="fa fa-users"></i> <span>{{ trans('modules.employees.employee_header') }}
        </a>
      </li>
    </ul>
  </section>
</aside>