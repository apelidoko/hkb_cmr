<header class="main-header">
  <!-- Logo -->
  <a href="{{ route('home') }}" class="logo">
    <span class="logo-mini">HKB</span>
    <span class="logo-lg">{{ config('app.name', 'Laravel') }}</span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">

        <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            
          </a>
          <ul class="dropdown-menu">
          </ul>
        </li>

        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user nav-icon"></i>
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">

            <li class="user-header">
              <i class="fa fa-user nav-icon"></i>
              <p>
                {{ Auth::user()->name }}
              </p>
            </li>

            <li class="user-footer">
              <div class="pull-right">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <button>{{ trans('modules.logout') }}</button>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
            </li>

          </ul>
        </li>

      </ul>
    </div>
  </nav>
</header>
  