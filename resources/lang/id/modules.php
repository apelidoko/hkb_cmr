<?php

return [

    'logout' => 'Keluar',
    'company' => [
        'company_header' => 'Perusahaan',
        'company_add' => 'Menambahkan Perusahaan',
    ],
    'employees' => [
        'employee_header' => 'Para karyawan',
        'employee_add' => 'Menambahkan Para karyawan',
    ],
    'menu' => 'TIDAK BISA',
    'home' => 'RUMAH',
    'email' => 'Alamat Email',
    'password' => 'Kata Sandi',
    'login' => 'Gabung',
];
