<?php

return [

    'logout' => 'Logout',
    'company' => [
        'company_header' => 'Companies',
        'company_add' => 'Add Company',
    ],
    'employees' => [
        'employee_header' => 'Employees',
        'employee_add' => 'Add Employee',
    ],
    'menu' => 'MENU',
    'home' => 'HOME',
    'email' => 'Email Address',
    'password' => 'Password',
    'login' => 'Login'

];
