Project Setup Guide

*** Note1 (Other Language Supported - Indonesia-ID) ***
<br>
*** Note2  (Notification will be sent only if new Company gets Added) ***

1. Run git clone https://gitlab.com/apelidoko/hkb_cmr.git
2. Go to Project directory and checkout **"master"** branch ('git checkout master') + ('git pull origin master' to pull latest changes)
3. Configure .env file 
	create database 'hkb_crm' 
	configure ['port', 'dbusername', 'dbpassword']
4. Run 'composer update'
5. Run 'composer dump-autoload'
6. Run 'php artisan migrate'
7. Run 'php artisan db:seed'
8. Run 'php artisan storage:link (This is to be able to display uploaded images in Public)'
9. Change Permission to /storage/app/public must be writable
10. Create .env.testing file
11. Run PHPUnit Test on project directory via commandline './vendor/bin/phpunit'
12. To Change Language (You may modify config/app.php variable 'locale' from 'en' to 'id') or via browser settings