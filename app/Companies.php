<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    public static function store($data, $fileName)
    {
    	$company = new Companies();
    	$company->name = $data['company_name'];
    	$company->email = $data['email'];
    	$company->website = $data['website'];
    	$company->logo = $fileName;
    	$company->save();
    }

    public static function updateCompany($data, $fileName)
    {
    	$company = [
    		'name' => $data['company_name'],
    		'email' => $data['email'],
    		'website' => $data['website']
    	];

    	if($fileName)
		{
    		$company['logo'] = $fileName;
		}

    	Companies::where('id', '=', $data['company_id'])->update($company);
    }
}
