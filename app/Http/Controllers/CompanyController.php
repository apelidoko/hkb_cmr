<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\EmailController;
use App\Companies;
use Response;
use Storage;



class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $companies = Companies::all();
        return view('companies', compact('companies'));
    }

    public function store(CompanyStoreRequest $request)
    {   
        /*=====================*/
        /* Validation Handler */
        /*===================*/
        if(!$request->validated())
        {   
           return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400); 
        }

        /*===============*/
        /* Store Image  */
        /*=============*/
        $fileName = "";
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $extension = strtoupper($file->getClientOriginalExtension());
            $fileName = time() . '.' . $extension;
            ImageController::store($request->file('logo'), $fileName);
        }

        /*========================*/
        /* Store Company Details */
        /*======================*/
        $storeCompany = Companies::store($request->all(),$fileName);

        /*=============*/
        /* Send Email */
        /*===========*/
        EmailController::sendMail($request->input('company_name'));

        return response()->json(array('success' => true), 200);
    }

    public function show($id)
    {
        return Companies::findOrFail($id);
    }

    public function update(CompanyStoreRequest $request, $id)
    {
        /*=====================*/
        /* Validation Handler */
        /*===================*/
        if(!$request->validated())
        {   
           return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400); 
        }

        /*===============*/
        /* Store Image  */
        /*=============*/
        $fileName = "";
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $extension = strtoupper($file->getClientOriginalExtension());
            $fileName = time() . '.' . $extension;
            ImageController::store($request->file('logo'), $fileName);
        }

        /*========================*/
        /* Store Company Details */
        /*======================*/
        $updateCompany = Companies::updateCompany($request->all(),$fileName);

        return response()->json(array('success' => true), 200);
    }

    public function destroy($id)
    {
        return Companies::find($id)->delete();
    }
}
