<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\CompanyNotification;
use App\Employees;

class EmailController extends Controller
{

	public static function sendMail($companyName)
	{
		$details = [
			'title' => 'Mail from HKB-CRM (Kevin del Rosario)',
			'body' => ' ('. strtoupper($companyName) .') Company has been added to the system by '. Auth::user()->name . ' .'
		];

		$receiver_emails = Employees::all()->pluck('email');

		Mail::to($receiver_emails)->send(new CompanyNotification($details));
	}


}

   
