<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\EmployeeStoreRequest;
use App\Companies;
use App\Employees;
use DB;
use Response;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = Employees::allWithCompany();
        $companies = Companies::all();
        return view('employees', compact('employees','companies'));
    }

    public function store(EmployeeStoreRequest $request)
    {
        /*=====================*/
        /* Validation Handler */
        /*===================*/
        if(!$request->validated())
        {   
           return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400); 
        }

        /*=========================*/
        /* Store Employee Details */
        /*=======================*/
        $storeEmployee = Employees::store($request->all());

        return response()->json(array('success' => true), 200);
    }

    public function show($id)
    {
        return Employees::findOrFail($id);
    }

    public function update(EmployeeStoreRequest $request, $id)
    {
        /*=====================*/
        /* Validation Handler */
        /*===================*/
        if(!$request->validated())
        {   
           return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400); 
        }

        /*=========================*/
        /* Store Employee Details */
        /*=======================*/
        $updateEmployee = Employees::updateEmployee($request->all());

        return response()->json(array('success' => true), 200);
    }
    
    public function destroy($id)
    {
        return Employees::find($id)->delete();
    }
}
