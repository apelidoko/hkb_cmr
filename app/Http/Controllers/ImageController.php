<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class ImageController extends Controller
{

    public static function store($logo, $fileName)
    {
        /* Store File to Public Folder */
        $path = $logo->storeAs('public/', $fileName);
    }
}