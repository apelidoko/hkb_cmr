<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'firstname' => 'required|string',
           'lastname' => 'required|string',
           'email' => 'email',
           'phone' => 'string'
        ];
    }

    public function messages()
    {
        return [
            'firstname.required' => 'First Name is Required!',
            'lastname.required' => 'Last Name is Required!',
            'email.email' => 'Invalid Email Format!',
            'phone.string' => 'Phone must not have special characters!'
        ];
    }
}
