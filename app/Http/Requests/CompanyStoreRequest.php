<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'company_name' => 'required|string',
           'email' => 'email',
           'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100'
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Company is required!',
            'email.email' => 'Invalid Email Format!',
            'logo.dimensions' => 'Image Minimum Dimension is 100x100!'
        ];
    }
}
