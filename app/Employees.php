<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employees extends Model
{
    public static function allWithCompany()
    {
    	return DB::SELECT("SELECT a.id as `employee_id`, a.firstname, a.lastname, a.email, a.phone, a.company_id, b.name 
                FROM `employees` AS a 
            INNER JOIN `companies` AS b ON b.id = a.company_id ");
    }

    public static function store($data)
    {
    	$employee = new Employees();
    	$employee->firstname = $data['firstname'];
    	$employee->lastname = $data['lastname'];
    	$employee->company_id = $data['company'];
    	$employee->email = $data['email'];
    	$employee->phone = $data['phone'];
    	$employee->save();
    }

    public static function updateEmployee($data)
    {
    	Employees::where('id', '=', $data['employee_id'])->update([
    		'firstname' => $data['firstname'],
    		'lastname' => $data['lastname'],
    		'company_id' => $data['company'],
    		'email' => $data['email'],
    		'phone' => $data['phone'],
    	]);
    }
}
