/* xlsx.js (C) 2013-present SheetJS -- http://sheetjs.com */
importScripts('shim.js');
/* uncomment the next line for encoding support */
importScripts('/vendors/jquery_sheetjs/dist/cpexcel.js');
importScripts('/vendors/jquery_sheetjs/dist/jszip.js');
importScripts('/vendors/jquery_sheetjs/dist/xlsx.js');
postMessage({t:"ready"});

onmessage = function (evt) {
  var v;
  try {
    v = XLSX.read(evt.data.d, {type: evt.data.b});
postMessage({t:"xlsx", d:JSON.stringify(v)});
  } catch(e) { postMessage({t:"e",d:e.stack||e}); }
};