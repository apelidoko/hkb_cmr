$(document).ready(function () {
  window._token = $('meta[name="csrf-token"]').attr('content')

  ClassicEditor.create(document.querySelector('.ckeditor'))

  moment.updateLocale('en', {
    week: {dow: 1} // Monday is the first day of the week
  })

  $('.date').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'en'
  })

  $('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    locale: 'en',
    sideBySide: true
  })

  $('.timepicker').datetimepicker({
    format: 'HH:mm:ss'
  })

  $('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
  })
  $('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
  })

  $('.select2').select2()

  $('.treeview').each(function () {
    var shouldExpand = false
    $(this).find('li').each(function () {
      if ($(this).hasClass('active')) {
        shouldExpand = true
      }
    })
    if (shouldExpand) {
      $(this).addClass('active')
    }
  });
  
});

function messageAlert(response_type,response){
  /* If Success */
  if(response_type=='success')
  {
    $.confirm({
      title: response,
      type: 'green',
      autoClose: 'close|2000',
      buttons: {
        close:{
          action: function () {
          }
        }
      }
    }); 
  }
  else if(response_type=='info'){ /* If Information */
    
    $.alert({
      title: 'Information:',
      type: 'blue',
      columnClass: 'medium',
      content: response
    }); 
  }
  else if(response_type=='confirm'){
    $.confirm({
      title: 'Confirm!',
      type: 'blue',
      content: response,
      buttons: {
        cancel: function () {
          
        },
        confirm: function () {
          $.alert('Confirmed!');
        }
      }
    });
  }
  else if(response_type=='error_timer')
  { 
    $.alert({
      title: 'Alert!',
      type: 'red',
      content: response,
      buttons: {
        confirm: function () {
          location.reload(true);
        }
      }
    }); 
  } 
  else /* If Error */
  { 
    $.alert({
      title: 'Alert!',
      type: 'red',
      content: response
    }); 
  } 
}

/**
* Global: Get Input Type
*/
function getType(elem)
{ 
  return elem[0].tagName == "INPUT" ? elem[0].type.toLowerCase() : elem[0].tagName.toLowerCase(); 
}

function getElemBoolVal($elem,elemType)
{
  var elemBoolVal;

  switch(elemType) {
        case 'file':
          elemBoolVal = $elem.val();
          break; 
        default:
          elemBoolVal = $elem.val();
     }

    return elemBoolVal;
}
