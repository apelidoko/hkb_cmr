<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

/* Default Page */
Route::get('/', 'HomeController@index');

/* Home Page */
Route::get('/home', 'HomeController@index')->name('home');

/* Companies and Emplyees Routes */
Route::resource('companies', 'CompanyController');

Route::resource('employees', 'EmployeeController');




