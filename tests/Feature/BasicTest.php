<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use App\User;
use Session;

class BasicTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    private $user; 
    private $password;

    public function testLoginIsAccessible()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function testUserCanAccessLoginFormWhenAuthenticated()
    {
        $this->user = factory(User::class)->make();
        $response = $this->actingAs($this->user)->get('/login');
        $response->assertRedirect('/home');
    }

    public function createDummyUser()
    {
        $this->password = 'password';
        $this->user = factory(User::class)->create([
            'password' => bcrypt($this->password),
        ]);
    }

    public function testUserCanLoginCorrectCredentials()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($this->user);
    }

    public function testUserCannotLoginWithIncorrectPassword()
    {
        $this->createDummyUser();

        $response = $this->from('/login')->post('/login', [
            'email' => $this->user['email'],
            'password' => 'invalid-password',
        ]);
        
        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertGuest();
    }


    public function testCompanyRouteRedirectGuest()
    {
        $response = $this->get('/companies');
        $response->assertStatus(302);
    }

    public function testCompanyRouteIsAccessibleByAuthenticatedUsers()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $response = $this->get('/companies');
        $response->assertStatus(200);
        $response->assertSuccessful();
        $response->assertViewIs('companies');
    }

    public function testEmployeeRouteRedirectGuest()
    {
        $response = $this->get('/employees');
        $response->assertStatus(302);
    }

    public function testEmployeeRouteIsAccessibleByAuthenticatedUsers()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => 'password',
        ]);

        $response = $this->get('/employees');
        $response->assertStatus(200);
        $response->assertSuccessful();
        $response->assertViewIs('employees');
    }

    public function testCompanySaveFormWithoutRequiredFields()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $company_name = 'Company123';
        $email = $company_name.'@a.com';
        $website = $company_name.'.com';

        $response = $this->json('POST','/companies', [
            'company_name' => '',
            'email' => '',
            'logo' => null,
            'file' => null,
            'website' => ''
        ]);
        $response->assertStatus(422);
    }

    public function testEmployeeSaveWithInvalidEmailFormat()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "Kevin";
        $lastname = "del Rosario";
        $email = "kevin@";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertJson([
            "errors" => [ 
                'email' => [
                    'Invalid Email Format!'
                ]
            ]
        ]);
    }

    public function testEmployeeSaveWithValidEmailFormat()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "Kevin";
        $lastname = "del Rosario";
        $email = "kevin@gmail.com";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertSuccessful();
    }

    public function testEmployeeSaveWithEmptyFirstnameField()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "";
        $lastname = "del Rosario";
        $email = "kevin@gmail.com";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertJson([
            "errors" => [ 
                'firstname' => [
                    'First Name is Required!'
                ]
            ]
        ]);
    }


    public function testEmployeeSaveWithValidFirstnameField()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "Kevin";
        $lastname = "del Rosario";
        $email = "kevin@gmail.com";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertSuccessful();
    }

    public function testEmployeeSaveWithEmptyLastnameField()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "Kevin";
        $lastname = "";
        $email = "kevin@gmail.com";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertJson([
            "errors" => [ 
                'lastname' => [
                    'Last Name is Required!'
                ]
            ]
        ]);
    }


    public function testEmployeeSaveWithValidLastnameField()
    {
        $this->createDummyUser();

        $response = $this->post('/login', [
            'email' => $this->user['email'],
            'password' => $this->password,
        ]);

        $firstname = "Kevin";
        $lastname = "del Rosario";
        $email = "kevin@gmail.com";
        $phone = "12345";
        $company_id = 1;

        $response = $this->json('POST','/employees', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company_id,
            'email' => $email,
            'phone' => $phone
        ]);

        $response->assertSuccessful();
    }

    


}
